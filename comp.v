Require Export bmc.

Open Scope string_scope.
Local Open Scope nat_scope.
Local Axiom by_smt : forall P : Prop, P.

Notation "[ ]" := nil.
Notation "[ x , .. , y ]" := (cons x .. (cons y []) ..).
Notation "x ++ y" := (app x y)
                     (at level 60, right associativity).

Fixpoint hold_state_body2 (si sj : state) (k : nat) : Prop :=
  match k with
  | O => True
  | S O => sj 0 = si 0
  | S k' => hold_state_body2 si sj k' /\ sj k' = si k'
  end.

Fixpoint hold_state_body1 (S : list nat) (si sj : nat -> state) (skip k : nat) : Prop :=
  match k with 
  | O => if beq_nat skip k then True
         else hold_state_body2 (si (k+1)) (sj (k+1)) (nth k S 0)
  | S k' => if beq_nat skip k then hold_state_body1 S si sj skip k'
            else hold_state_body2 (si (k+1)) (sj (k+1)) (nth k S 0) /\
                 hold_state_body1 S si sj skip k'

  end.

Fixpoint hold_state_list (S : list nat) (si sj : nat -> state) (n k : nat) : list Prop :=
  match n with
  | O => [hold_state_body1 S si sj n k]
  | S n' => hold_state_list S si sj n' k
                            ++ [hold_state_body1 S si sj n k]
  end.

Fixpoint comp_T_body (T H : list Prop) : Prop :=
  match T, H with
  | h :: nil, h' :: nil => h /\ h'
  | h :: t,   h' :: t'  => h /\ h' \/ comp_T_body t t'
  | _, _ => False
  end.

Fixpoint alloc_state (T : list sub_trans)
         (si sj : nat -> state) (n : nat) : list Prop :=
  match T with
  | nil => []
  | h :: nil => [h (si n) (sj n) (si 0) (sj 0)]
  | h :: t => [h (si n) (sj n) (si 0) (sj 0)]
                ++ alloc_state t si sj (n+1)
  end.


Definition comp_T (T_list : list sub_trans) (size_list : list nat)
           (si sj : nat -> state) : Prop :=
  comp_T_body (alloc_state T_list si sj 1)
              (hold_state_list size_list si sj (length T_list - 1) (length T_list - 1)).



(* comp_T の説明
 ・引数の説明
    T : [(T1, N1), (T2, N2) .., (Tn, Nn)]
        Tx : sub_trans
        Nx : nat
    si sj は, nat -> nat -> R
              1つ目のnatは、サブシステムの番号
              2つ目のnatは、各サブシステムの状態の要素番号(状態がベクトルになっている)
 ・comp_T_body
    この関数の中で実際に合成を行っている.
    ・sub_trans_list
       [1番目のサブシステムの（状態の）遷移関係,
        2番目のサブシステムの遷移関係
        ...
        n番目のサブシステムの状態関係]
        というリストを返す関数
        ※プロセス番号に合わせて具体的な状態の割り当ても行っている。

    ・hold_state_list
       [1番目以外のサブシステムの状態を保持,
        2番目以外のサブシステムの状態を保持,
        ...
        n番目以外のサブシステムの状態を保持]
      というリストを返す関数.
 *)



Fixpoint comp_I_body (I : list sub_init) (s : nat -> state) (n : nat) : Prop :=
  match I with
  | nil => True
  | a :: nil =>  a (s n) (s 0)
  | a :: t => a (s n) (s 0) /\ comp_I_body t s (n+1)
  end.

Definition comp_I (I : list sub_init) (s : nat -> state) : Prop :=
 comp_I_body I s 1.


