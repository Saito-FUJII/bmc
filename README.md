coq-bmc
=============


Requirements
-------

 - [Coq8.5.1](https://coq.inria.fr/coq-86)
 - [coq-smt-check](https://github.com/gmalecha/coq-smt-check)
 - [coq-plugin-utils](https://github.com/gmalecha/coq-plugin-utils)

