Require Import Coq.Strings.String.
Require Import Coq.Reals.Rdefinitions.
Require Import Coq.Reals.RIneq.
Require Export Coq.fourier.Fourier.
Require Import Coq.Logic.Classical_Prop.
Require Import Coq.omega.Omega.
Require Import Compare_dec.
Require Export Coq.Lists.List.
Require Export Arith.
Require Export Arith.EqNat. 
Require Import SMTC.Tactic.


Require Export bmc.
Open Scope string_scope.

Set SMT Solver "z3".
Set SMT Debug.

Local Open Scope nat_scope.
Local Axiom by_smt : forall P : Prop, P.


Fixpoint all_not_I (I : init) (s : ss) (o len : nat) : Prop :=
  match len with
  | O => True
  | S O => ~ I (s (len + o))
  | S len' => all_not_I I s o len' /\ ~ I (s (len' + o))
  end.



Fixpoint big_and (P : nat -> Prop) (o num : nat) : Prop :=
  match num with
  | O => True
  | S O => P o
  | S num' => big_and P o num' /\ P (num' + o)
  end.

Definition all_P (P : property) (s : ss) (o num : nat) : Prop :=
  big_and (fun n => P (s n)) o num.


Definition Sheeran_method2 (I : init) (T : trans) (P : property)
           (size_list : list nat) (k : nat) : Prop :=
  ((forall s1 : ss,  lasso I T s1 0 k size_list (*\/ all_not_I I s1 1 k*) ) \/
   (forall s2 : ss, violate_loop_free T P s2 0 k size_list \/ ~ all_P P s2 0 k)) /\
  kth_P_safe I T P k.

Lemma case2_t1' : forall (i : nat) (P : property),
    (forall s : ss, P (s 0)) <-> (forall s : ss, P (s i)).
Proof.
Admitted.

Lemma classical_prop1 :
  forall A B C, ~ (A /\ B /\ ~ C) <-> (A /\ B -> C).
Proof.
  split.
  intros.
  tauto.
  intros.
  tauto.
Qed.

Lemma xxxxx : forall (T : trans) (P : property) (l : list nat) (i k : nat),
 i >= S k ->
    (forall s2 : ss, loop_free T s2 0 (S k) l /\ all_P P s2 0 (S k) -> P (s2 (S k)))
    <->
    (forall s2 : ss, loop_free T s2 (i-k-1) (S k) l /\ all_P P s2 (i-k-1) (S k) -> P (s2 i)).
Proof. Admitted.

Lemma yyyy : forall (s : ss) (I : init) (T : trans) (P : property) (l : list nat) (i k : nat),
    S k <= i -> (forall m : nat, m < i -> I (s 0) /\ loop_free T s 0 m l -> P (s m))
    ->  all_P P s (i - k - 1) (S k).
Proof. Admitted.



Theorem Proof_Sheeran_method2 :
  forall (k : nat) (I : init) (T : trans) (P : property) (l : list nat),
    Sheeran_method2 I T P l k
    -> (forall (i : nat) (s : ss),
           ~ (I (s 0) /\ loop_free T s 0 i l /\ ~ P (s i))).
Proof.
  intros.
  apply classical_prop1.
  intros.

  induction i using lt_wf_ind.
  unfold Sheeran_method2 in H.
  intros.
  destruct (Nat.lt_ge_cases i k).
  + assert (H3 : (i < k) /\ kth_P_safe I T P k).
    tauto.
    apply case1_t1 with (s := s) in H3.  
    apply ->  classical_prop1 in H3.
    tauto.
    unfold loop_free in H0.
    tauto.
  + destruct H.
    destruct H.
    - assert (H4 : i =  k + (i - k)). (*i >= k より自明*)
      omega.
      unfold lasso in H.
      destruct H0.
      rewrite H4 in H5.
      apply divide_loop_free in H5.
      firstorder.
    - unfold violate_loop_free in H.
      destruct k.
      * unfold loop_free in H.
        simpl in *.
        assert (H4 : forall s2 : ss, P (s2 0)).
        firstorder.
        rewrite case2_t1' with (P := P) (i := i) in H4.
        firstorder.
      * assert (H5 : i = (i - k - 1) + (k + 1)).
        omega.
        assert (H10 : loop_free T s 0 i l).
        tauto.

        rewrite H5 in H0.
        destruct H0.

        apply divide_loop_free in H4.

        generalize H2.
        intros.
        apply xxxxx with (T := T) (P := P) (l := l) in H6.
        rewrite plus_O_n in H.
        assert (H7 : forall s2 : ss,
                   loop_free T s2 0 (S k) l /\ all_P P s2 0 (S k) -> P (s2 (S k))).
        firstorder.
        clear H.
        rewrite H6 in H7.
        apply H7.
        split.
        rewrite Nat.add_1_r in H4.
        tauto.

        apply (yyyy s I T P l) in H2.
        apply H2.
        apply H1.
Qed.



