Require Import Coq.Strings.String.
Require Import Coq.Reals.Rdefinitions.
Require Import Coq.Reals.RIneq.
Require Export Coq.fourier.Fourier.
Require Import Coq.Logic.Classical_Prop.
Require Import Coq.omega.Omega.
Require Import Compare_dec.
Require Export Coq.Lists.List.
Require Export Arith.
Require Export Arith.EqNat. 
Require Import SMTC.Tactic.

Open Scope string_scope.

Set SMT Solver "z3".
Set SMT Debug.

Local Open Scope nat_scope.
Local Axiom by_smt : forall P : Prop, P.


Definition state := nat -> Z.
Definition ss := nat -> nat -> state.
Definition sub_trans := state -> state -> state -> state -> Prop.
Definition sub_init := state -> state -> Prop.
Definition trans :=  (nat -> state) -> (nat -> state) -> Prop.
Definition init := (nat -> state) -> Prop.
Definition property := (nat -> state) -> Prop.


Fixpoint path
 (t : trans) 
 (s : ss) (o len : nat) : Prop :=
 match len with
 | 0 => True
 | 1 => t (s (len + o - 1)) (s (len + o)) 
 | S len' => path t s o len' /\ 
             t (s (len' + o)) (s (len + o))
 end.

Fixpoint naive_method (I : init) (T : trans) (P : property)
         (s : ss) (k : nat) : Prop :=
  match k with 
  | O => ~ (I (s 0) /\ path T s 0 k /\ ~ P (s k))
  | S k' => naive_method I T P s k'
            /\ ~ (I (s 0) /\ path T s 0 k /\ ~ P (s k))
  end.


Fixpoint neq_nth_mth_body (si sj : state) (size: nat) : Prop :=
 match size with
  | 0 => True
  | 1 => ~ (si 0 = sj 0)
  | S s' => neq_nth_mth_body si sj s' \/ ~ (si s' = sj s')
 end.


Fixpoint neq_nth_mth (si sj : nat -> state)
         (size_list : list nat) (n : nat)  : Prop :=
  match size_list with
  | nil => False
  | h :: nil => neq_nth_mth_body (si n) (sj n) h
  | h :: t => neq_nth_mth_body (si n) (sj n) h
              \/ neq_nth_mth si sj t (n+1)
  end.
    

Fixpoint loop_check' (s : ss) (o n m : nat) (size_list : list nat) : Prop :=
  match m with
  | O => neq_nth_mth (s (o+n)) (s (o+m)) size_list 0
  | S m' => neq_nth_mth (s (o+n)) (s (o+m)) size_list 0
            /\ loop_check' s o n m' size_list
  end.

Fixpoint loop_check (s : ss) (o n : nat) (size_list : list nat) : Prop :=
  match n with
  | O => True
  | 1 => loop_check' s o 1 0 size_list
  | S m => loop_check' s o n m size_list /\ loop_check s o m size_list
  end.


Definition loop_free (T : trans) (s : ss)
           (o n : nat) (size_list : list nat) : Prop :=
  (path T s o n /\ loop_check s o n size_list).

Definition lasso (I : init) (T : trans)
           (s : ss) (o i : nat) (size_list : list nat) : Prop :=
  ~ I (s 0) \/ ~ loop_free T s o i size_list.


Definition violate_loop_free (T : trans) (P : property)
           (s : ss) (o i : nat) (size_list : list nat) : Prop :=
  ~ loop_free T s o i size_list \/ P (s (o+i)).


Fixpoint big_and (P : nat -> Prop) (o num : nat) : Prop :=
  match num with
  | O => True
  | S O => P o
  | S num' => big_and P o num' /\ P (num' + o)
  end.

Definition kth_P_safe (I : init) (T : trans) (P : property) (k :nat) : Prop :=
  big_and (fun n : nat =>
             forall s : ss, ~ (I (s 0) /\ path T s 0 n  /\ ~ P (s n))) 0 (S k).


Definition Sheeran_method1 (I : init) (T : trans) (P : property)
           (size_list : list nat) (k : nat) : Prop :=
  ((forall s1 : ss,  lasso I T s1 0 k size_list) \/
   (forall s2 : ss, violate_loop_free T P s2 0 k size_list)) /\
  kth_P_safe I T P k.


(*
Tactic Notation "Sheeran_smt_solve1" :=
 unfold Sheeran_method1;
 unfold lasso; unfold violate_loop_free; unfold kth_P_safe;
 unfold loop_free;
 simpl;
 repeat tryif split then try split else
     tryif right; intros; smt solve then apply by_smt
     else  left; intros; smt solve; apply by_smt.
*)

Lemma divide_tl_path : forall (i : nat) (s : ss) ( T : trans),
        path T s 0 (S i) <-> path T s 0 i /\ T (s i) (s (S i)).
Proof.
  intros. 
  destruct i. 
  - unfold path.
    tauto.
  - unfold path; fold path; simpl.
    rewrite <- plus_n_O.
    rewrite <- minus_n_O.
    tauto.
Qed. (*8*)


Lemma divide_hd_path : forall (i j : nat) (s : ss) (T : trans),
        T (s i) (s (S i)) /\ path T s (S i) j <-> path T s i (S j).
Proof.
  destruct j.
  - unfold path. simpl. rewrite <- minus_n_O. tauto.
   
  - induction j. simpl. 
    rewrite <- minus_n_O. tauto.
    simpl. 
    split; firstorder; now rewrite Nat.add_succ_r in *.
Qed. (*13*)


Lemma shift_path : forall (i j : nat) (s : ss) (T : trans), 
  path T s 0 i /\ path T s i (S j) <-> 
  path T s 0 (S i) /\ path T s (S i) j .
Proof.
  intros.
  rewrite divide_tl_path.
  rewrite and_assoc.
  rewrite divide_hd_path.
  reflexivity.
Qed. (*5*)

Lemma divide_path : forall (i j: nat) (s : ss) (T : trans),
          path T s 0 (i+j) -> path T s 0 i /\ path T s i j.
Proof.
  induction i.
  - simpl.
    tauto.

  - intros.
    rewrite -> Nat.add_succ_comm in H.
    apply IHi in H.
    apply shift_path.
    apply H.
Qed. (*8*)


Lemma divide_lc1 : forall (j i : nat) (s : ss) (l : list nat), 
        loop_check s 0 (i+j) l -> loop_check s 0 i l.
Proof.
  induction j.
  - intros.  rewrite <- plus_n_O in H.
    easy.
  - intros.
    rewrite <- Nat.add_succ_comm in H.
    apply IHj in H.
    assert(loop_check s 0 (S i) l <-> loop_check s 0 i l /\
                                    loop_check' s 0 (S i) i l)
      by (destruct i; firstorder).
    apply H0 in H.
    firstorder.
Qed. (*12*)

Lemma divide_lc2'' : forall (i j k : nat) (s : ss) (l : list nat),
        loop_check' s i (S k) (S j) l <->
        neq_nth_mth (s (i + (S k))) (s i) l 0 /\  loop_check' s (S i) k j l.
Proof.
  destruct j.
  - simpl. rewrite <-  plus_n_O.
    intros.
    rewrite <- Nat.add_succ_l.
    rewrite Nat.add_succ_comm.
    rewrite Nat.add_1_r.
    tauto.
  - induction j.
    + simpl.
      intros.
      now rewrite <- Nat.add_succ_r; rewrite <- Nat.add_succ_r;
        rewrite <-  Nat.add_1_r; rewrite <- (plus_n_O i);
          rewrite <- (Nat.add_1_r i).
    + intros.
      simpl in *.
      assert(neq_nth_mth (s (i + S k)) (s i) l 0 /\
            neq_nth_mth (s (S (i + k))) (s (S (i + S (S j)))) l 0 /\
            neq_nth_mth (s (S (i + k))) (s (S (i + S j))) l 0 /\
            loop_check' s (S i) k j l <->
            (neq_nth_mth (s (i + S k)) (s i) l 0 /\
            neq_nth_mth (s (S (i + k))) (s (S (i + S j))) l 0 /\
            loop_check' s (S i) k j l) /\
            neq_nth_mth (s (S (i + k))) (s (S (i + S (S j)))) l 0) by tauto.
     now rewrite H; rewrite <- IHj; rewrite <- Nat.add_succ_r;
        rewrite <- Nat.add_succ_r.
Qed. (*26*)


Lemma divide_lc2' : forall (j i : nat) (s : ss) (l : list nat),
    loop_check s i (S j) l -> loop_check s (S i) j l.
Proof.
  induction j.
  intros.
  - destruct i; firstorder.
  - intros.
    assert(loop_check s (S i) (S j) l <->
           loop_check' s (S i) (S j) j l /\ loop_check s (S i) j l)
      by (destruct j; firstorder).
    apply H0.
    assert(loop_check s i (S (S j)) l <->
           loop_check' s i (S (S j)) (S j) l /\ loop_check s i (S j) l)
      by (destruct j; firstorder).
    apply -> H1 in H.
    destruct H.    
    split. 
    now apply divide_lc2'' in H.
    now apply IHj in H2.
Qed. (*19*)


Lemma divide_lc2 : forall (i j : nat) (s : ss) (l : list nat),
    loop_check s 0 (i+j) l -> loop_check s i j l.
Proof.
  induction i.
  - easy.
  - intros.
    rewrite Nat.add_succ_comm in H.
    apply IHi in H.
    now apply divide_lc2' in H.
Qed. (*7*)


Lemma divide_loop_check : forall (i j: nat) (s : ss) (l : list nat),
    loop_check s 0 (i+j) l ->
    loop_check s 0 i l /\ loop_check s i j l.
Proof.
  intros.
  split.
  - now apply divide_lc1 in H.
  - now apply divide_lc2 in H.
Qed. (*6*)

Theorem divide_loop_free : forall (i j: nat) (s : ss) (T : trans) (l : list nat),
    loop_free T s 0 (i+j) l -> loop_free T s 0 i l /\ loop_free T s i j l.

Proof.
  unfold loop_free.
  intros.
  destruct H.
  now apply divide_path in H; apply divide_loop_check in H0.
Qed. (*6*)

Lemma lt_big_and_incl : forall (i k : nat) (P : nat -> Prop),
    i < k /\ big_and P 0 (S k) -> big_and P 0 (S i).
Proof.
  intros.
  induction k.
  - easy. 

  - destruct H.
    destruct (Nat.lt_ge_cases i k).
    + assert (big_and P 0 (S k) /\ P (S k))
        by (firstorder; now rewrite <- plus_n_O in *).
      now apply IHk.
    + apply gt_S_le in H.
      assert (i = k) by omega.
      rewrite H2; firstorder.
Qed. (*12*)


Lemma case1_t1 : forall (i k : nat) (s : ss) (I : init) (T : trans) (P : property),
    (i < k) /\ kth_P_safe I T P k ->
    ~ (I (s 0) /\ path T s 0 i /\ ~ P (s i)).
Proof.
  intros.
  unfold kth_P_safe in H.
  apply lt_big_and_incl in H.
  destruct i; firstorder; now rewrite <- plus_n_O in *.
Qed. (*6*)

Theorem Proof_Sheeran_method1_case1 :
  forall (k : nat) (I : init) (T : trans) (P : property) (l : list nat),
    Sheeran_method1 I T P l k
    -> (forall (i : nat) (s : ss), 
           (i < k)%nat
           -> ~ (I (s 0) /\ loop_free T s 0 i l /\ ~ P (s i))).
Proof.
  unfold Sheeran_method1.
  intros.
  assert(H1 : (i < k) /\ kth_P_safe I T P k) by easy.
  apply case1_t1 with (s := s) in H1.  
  unfold loop_free.
  tauto.
Qed. (*7*)


Lemma ss_prop : forall (X : (nat -> state) -> Prop) (i j : nat),
    (forall s : ss, X (s i)) <-> (forall s : ss, X (s j)).
Proof. Admitted.


Lemma case2_t1 : forall (i k : nat) (T : trans) (P : property) (l : list nat),
 (i >= k)%nat ->
  (forall s1 : ss, ~ loop_free T s1 (i-k) k l \/ P (s1 i)) <-> 
  (forall s2 : ss, ~ loop_free T s2 0 k l \/ P (s2 k)).
Proof.
  intros.
  split.
  intros.
  assert (forall s1' : ss, ~ loop_free T s1' 0 k l \/ P (s1' k)). {
       apply by_smt.
  }

  auto.

Admitted.

  (* i >= k について *)
Theorem Proof_Sheeran_method1_case2 :
  forall (k : nat) (I : init) (T : trans) (P : property) (l : list nat),
    Sheeran_method1 I T P l k
    -> (forall (i : nat) (s : ss),
           (i >= k)%nat -> ~ (I (s 0) /\ loop_free T s 0 i l /\ ~ P (s i))).
Proof.
  unfold Sheeran_method1.
  intros.
  apply neg_false.
  split. 
  - intros.
    destruct H.
    destruct H.
    + assert (H3 : i =  k + (i - k)) by omega.  (*i >= k より自明*)
      unfold lasso in H.
      destruct H1.
      destruct H4.   
      rewrite H3 in H4.
      apply divide_loop_free in H4.
      firstorder.    
    + unfold violate_loop_free in H.
      simpl in H.
      destruct H1.
      destruct H3.
      assert (H5 : i = i - k + k) by omega.  (*i >= k より自明*)
      rewrite H5 in H3.
      apply divide_loop_free in H3.
      apply (case2_t1 i k T P l) in H0.
      rewrite <- H0 in H.
      firstorder.
  - easy.
Qed. (*27*)


Theorem Proof_Sheeran_method1 :
  forall (k : nat) (I : init) (T : trans) (P : property) (l : list nat),
    Sheeran_method1 I T P l k
    -> (forall (i : nat) (s : ss),
           ~ (I (s 0) /\ loop_free T s 0 i l /\ ~ P (s i))).
Proof.
  intros.
  destruct (Nat.lt_ge_cases i k).
  - revert H0.
    apply Proof_Sheeran_method1_case1.
    easy.
  - revert H0.
    apply  Proof_Sheeran_method1_case2.
    easy.
Qed.  (*8*)  




