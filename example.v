Require Import Coq.Strings.String.
Require Import Coq.Reals.Rdefinitions.
Require Import Coq.Reals.RIneq.
Require Import Coq.Logic.Classical_Prop.

Require Import SMTC.Tactic.

Set SMT Solver "z3".
Set SMT Debug.

Local Open Scope nat_scope.
Local Axiom by_smt : forall P : Prop, P.


Require Export comp.
Local Open Scope nat_scope.



(* Semaphore-based mutual exclusion

     PG of each process : 

                      |
                      V
                    *---------*
                --> | noncrit |
               |    *---------*
               |         |
               |         V
               |    *---------*
   g := g + 1  |    |  wait   |
               |    *---------*
               |         |  g > 0 : 
               |         V  g := g - 1
               |    *---------*
                --- |  crit   |
                    *---------*

  "g" is a semaphore argument.
  Actually, each state is represented as real values, 0(noncrit), 1(wait) or
  2(crit) in following process_I and process_T.

 *)

Definition process_I (s g: state)  : Prop :=
  s 0 = 0%R /\ g 0 = 1%R.

Definition process_T (si sj gi gj : state) : Prop :=
 (si 0 = 0%R /\ sj 0 = 1%R /\ gj 0 = gi 0 /\ sj 0 = 1%R \/
                                              
  si 0  = 1%R /\ 
  (gi 0%nat  > 0)%R /\ gj 0 = (gi 0%nat - 1)%R /\ sj 0 = 2%R  \/
                                               
  si 0 = 1%R /\ ~ (gi 0%nat > 0)%R /\ gj 0 = gi 0 /\ sj 0  = si 0 \/

  si 0  = 2%R /\ sj 0  = 0%R /\  gj 0 = (gi 0%nat + 1)%R).


Tactic Notation "Sheeran_smt_solve1" :=
 unfold Sheeran_method1;
 unfold lasso; unfold violate_loop_free; unfold kth_P_safe;
 unfold kth_P_safe_body; unfold loop_free;
 simpl;
 repeat tryif split then try split else
     tryif right; intros; smt solve then apply by_smt
     else  left; intros; smt solve; apply by_smt.


(*Running a example*)

Definition Ex_T :=
  comp_T [process_T, process_T] [1, 1, 1].


Definition Ex_I :=
  comp_I [process_I, process_I].

Definition Ex_P (s : nat -> state) : Prop :=
  ~ (s 1 0 = 2%R /\ s 2 0 = 2%R).


Example ex1 :
  Sheeran_method1 Ex_I Ex_T Ex_P [1, 1, 1] 10.
Proof.
  unfold Ex_I; unfold Ex_T; unfold Ex_P.
  unfold comp_T; unfold comp_I;
  simpl.
  unfold process_T; unfold process_I.
  Sheeran_smt_solve1.
Qed.


  (*

Definition SR_I (s g : state) : Prop :=
 s 2 = 1%R /\ s 1 = 1%R /\ s 0 = 0%R.
 
Definition SR_T (si sj gi gj: state) : Prop :=
  sj 2 = 1%R /\ sj 1 = si 2 /\
  sj 0 = si 1.

Definition SR_P (s : nat -> state) : Prop :=
 ~ (s 1 2 = 0%R /\ s 1 1 = 0%R /\ s 1 0 = 0%R).
 


(*****************************************************************)
(*自動販売機の例 (プロトタイプ): start select をそれぞれ 0 1 として扱う.
  [start or select, ソーダの本数, ビールの本数]  *)

Definition VM_I  (s g : state)  : Prop :=
 s 0 = 0%R /\ s 1 = 2%R /\ s 2 = 2%R.

Definition VM_T (si sj gi gj: state) : Prop :=
 si 0 = 0%R /\ sj 0 = 1%R /\    
 sj 1 = si 1 /\ sj 2 = si 2 \/ 

 si 0 = 1%R /\ (si 1 = 0%R /\ si 2 = 0%R) /\ sj 0 = 0%R /\
 sj 1 = si 1 /\ sj 2 = si 2 \/

 si 0 = 1%R /\ ~ (si 1 = 0%R) /\ sj 0 = 0%R /\ 
 sj 1 = (si 1%nat - 1)%R /\ sj 2 = si 2 \/
 
 si 0 = 1%R /\ ~ (si 2 = 0%R) /\ sj 0 = 0%R /\
 sj 1 = si 1 /\ sj 2 = (si 2%nat - 1)%R.
 
Definition VM_P (s : nat -> state) : Prop :=
  ~(s 1%nat 1%nat < 0 \/ s 1%nat 2%nat < 0)%R.
  

(*****************************************************************)
(*ピーターソンの排他制御の例 (principle of model checking, Example 2.25): 
  状態 noncrit wait crit をそれぞれ 0 1 2
  x は 1 or 2
  b = 0 or 1
  b は各プロセスが動作しているかの判定
  [proc0の状態, proc1の状態, x, b1, b2]
*)


Definition process0 (si sj: state) (x b1 b2: nat) : Prop :=
 si 0 = 0%R /\ sj 0 = 1%R /\ 
 sj b1 = 1%R /\ sj b2 = si b2 /\ sj x = 2%R \/

 si 0 = 1%R /\ sj 0 = 2%R /\ 
 (si x = 1 \/ si b2 = 0)%R /\ 
 sj b1 = si b1 /\ sj b2 = si b2 /\ sj x = si x \/
 
 si 0 = 1%R /\ sj 0 = 1%R /\ 
 ~ (si x = 1 \/ si b2 = 0)%R /\
 sj b1 = si b1 /\ sj b2 = si b2 /\ sj x = si x \/

 si 0 = 2%R /\ sj 0 = 0%R /\ 
 sj b1 = 0%R /\ sj b2 = si b2 /\ sj x = si x. 


Definition process1 (si sj: state) (x b1 b2 : nat) : Prop :=
 si 1 = 0%R /\ sj 1 = 1%R /\ 
 sj b2 = 1%R /\ sj b1 = si b1 /\ sj x = 1%R \/

 si 1 = 1%R /\ sj 1 = 2%R /\ 
 (si x = 2 \/ si b1 = 0)%R /\ 
 sj b1 = si b1 /\ sj b2 = si b2 /\ sj x = si x \/
 
 si 1 = 1%R /\ sj 1 = 1%R /\ 
 ~ (si x = 2 \/ si b1 = 0)%R /\
 sj b1 = si b1 /\ sj b2 = si b2 /\ sj x = si x \/

 si 1 = 2%R /\ sj 1 = 0%R /\ 
 sj b2 = 0%R /\ sj b2 = si b2 /\ sj x = si x.


Definition Peterson_I (s : state) : Prop :=
 s 0 = 0%R /\ s 1 = 0%R /\ s 3 = 0%R /\ s 4 = 0%R /\  
 (s 2 = 1%R  \/ s 2 = 2%R).
 
Definition Peterson_T (si sj : state) : Prop :=
 process0 si sj 2 3 4 /\ sj 1 = si 1 \/
 process1 si sj 2 3 4 /\ sj 0 = si 0.
 
Definition Peterson_P (s : state) : Prop :=
 ~ (s 0 = 2%R /\ s 1 = 2%R).
 
Definition Peterson_size : nat := 5.

 *)